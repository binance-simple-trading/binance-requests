# Binance Requests

## __Description__:
Simple requests library for binance public and signed API calls. Public API calls are used for public information, such as price data. Signed API calls are used to manage an account automatically, such as buying or selling an asset.

The API is based on https://github.com/binance/binance-spot-api-docs/blob/master/rest-api.md.

The API information must be set up on your Binance account as described here: https://www.binance.com/en-NG/support/faq/360002502072

## __Usage__:
```
binance = BinanceRequests()
# or
binance = SignedBinanceRequests("/path/to/api.json")
binance.makeRequestRaw(url, params) # Returns requests object
binance.makeRequest(url, params) # Returns data as list/dict
```

The api.json file must be in format:
```
{
    "secretKey" : "...",
    "apiKey" : "..."
}
```

## __Example__:
See [binance-examples](https://www.gitlab.com/binance-simple-trading/binance-examples) for more.

- Get some price information
```
binance = BinanceRequests()
req = binance.makeRequest("/api/v3/avgPrice", params={"symbol" : "DOGEBUSD"})
# A dict of current DOGE x BUSD price
data = req["data"]
```

- Get account balance
```
binance = SignedBinanceRequests("/path/to/api.json")
req = binance.makeRequest("/api/v3/account", params={})
# An array of your curent balance
balance = req["data"]["balances"]
# A dict with 2 entries: "sendTimestamp" and "recvTimestamp"
timestamp = req["timestamps"]
```