from typing import Dict
from .signed_binance_request import SignedBinanceRequest
from .binance_request import BinanceRequest

# Wrapper class that does API calls regarding user data.
class BinanceClient:
    def __init__(self, secretFilePath:str):
        self.secretFilePath = secretFilePath
        self.unsignedBinance = BinanceRequest()
        if secretFilePath is None:
            print("[BinanceClient] No secret file path was provided. Can only do public requests!")
            self.binance = self.unsignedBinance
        else:
            self.binance = SignedBinanceRequest(secretFilePath=secretFilePath)
            # Validate that the account is legit.
            _ = self.makeRequest("/api/v3/account", params={}, requestType="GET", signed=True)

    # @brief Makes an signed or unsigned request. If signed, the client must be instantiated with a API key. If
    #  usigned, it will check whether the client is signed or not and call the appropriate (w/ or w/o timestamp)
    #  method.
    def makeRequest(self, apiCall, params, requestType:str, signed:bool=True):
        if signed:
            assert not self.secretFilePath is None, "Call to signed request using unsigned client."
        client = self.binance if signed else self.unsignedBinance
        req = client.makeRequest(apiCall, params=params, headers={}, requestType=requestType)
        return req

    # @brief Returns all the current prices of the entire binance network
    def getAllPrices(self):
        req = self.makeRequest("/api/v3/ticker/price", params={}, requestType="GET", signed=False)["data"]
        res = {}
        for i in range(len(req)):
            k, v = req[i]["symbol"], float(req[i]["price"])
            res[k] = v
        res["BUSDBUSD"] = 1.0
        return res

    # @brief Returns the balance of the client only for the coin's it's invested in. 
    def getAccountBalance(self):
        allPrices = self.getAllPrices()
        req = self.makeRequest("/api/v3/account", params={}, requestType="GET", signed=True)
        balances = req["data"]["balances"]
        result = {"timestamps": req["timestamps"], "updateTime": req["data"]["updateTime"], "balance": {}}
        for balance in balances:
            asset, free, locked = balance["asset"], float(balance["free"]), float(balance["locked"])
            if free == 0 and locked == 0:
                continue

            busdValue = 1 if asset == "BUSD" else allPrices[f"{asset}BUSD"]
            assetBusd = (free + locked) * busdValue
            result["balance"][asset] = (free, locked, assetBusd)
        return result

    # @brief Returns all open orders for a particular symbol (eg: "BTTBUSD")
    def getOpenOrder(self, symbol:str):
        req = self.makeRequest("/api/v3/openOrders", params={"symbol": symbol}, requestType="GET", signed=True)["data"]
        return req

    # @brief Returns all open orders
    def getAllOpenOrders(self):
        req = self.makeRequest("/api/v3/openOrders", params={}, requestType="GET", signed=True)["data"]
        return req

    def openNewOrder(self, symbol:str, side:str, price:float, quantity:float):
        assert side in ("BUY", "SELL")
        params = {
            "symbol": symbol,
            "side": side,
            "type": "LIMIT",
            "timeInForce": "GTC",
            "quantity": quantity,
            "price": price,
        }
        req = self.makeRequest("/api/v3/order", params=params, requestType="POST", signed=True)
        data = req["data"]
        return data

    def __str__(self):
        Str = "[BinanceClient]"
        Str += "\n - API path: %s" % self.secretFilePath
        return Str