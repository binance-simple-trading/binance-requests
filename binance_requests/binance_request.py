import requests
import time
from typing import Dict
from .logger import logger

class BinanceRequest:
    def __init__(self):
        self.apiURL = "https://api.binance.com"

    def prepare(self, apiCall:str, params:Dict, headers:Dict):
        assert apiCall.startswith("/api"), "Wrong format for API call"
        url = f"{self.apiURL}{apiCall}"
        logger.debug2(f"\n - URL: {url}.\n - Params: {params}.\n - Headers: {headers}")
        return url, params, headers

    def makeRequestRaw(self, apiCall:str, params:Dict, headers:Dict, requestType:str):
        assert requestType in ("POST", "GET")
        logger.debug(f"Request type: {requestType}")

        url, params, headers = self.prepare(apiCall, params, headers)
        sendTimestamp = int(time.time() * 1000)
        reqF = requests.get if requestType == "GET" else requests.post
        req = reqF(url, params=params, headers=headers)
        logger.debug(f"Received respose. Status Code: {req.status_code}")
        assert req.status_code == 200, req.text
        req.timestamps = {"sendTimestamp":sendTimestamp, "recvTimestamp":int(time.time() * 1000)}
        return req

    def makeRequest(self, apiCall:str, params:Dict, headers:Dict, requestType:str):
        req = self.makeRequestRaw(apiCall, params, headers=headers, requestType=requestType)
        timestamps = req.timestamps
        diff = req.timestamps["recvTimestamp"] - req.timestamps["sendTimestamp"]
        logger.debug2(f"Timetamps: Send: {timestamps['sendTimestamp']} | Recv: {timestamps['recvTimestamp']} | " + \
            f"Diff: {diff} (ms)")

        reqJson = req.json()
        data = [dict(x) for x in reqJson] if isinstance(reqJson, list) else dict(reqJson)
        result = {"data":data, "timestamps":timestamps}
        return result
