import hmac
import hashlib
import json
import time
from typing import Dict
from overrides import overrides
from urllib.parse import urlencode
from .binance_request import BinanceRequest
from .logger import logger

class SignedBinanceRequest(BinanceRequest):
    def __init__(self, secretFilePath:str):
        super().__init__()
        self.secretFilePath = secretFilePath
        self.secret = json.loads(open(secretFilePath, "r").read())

    def getRequestSignature(secretKey:str, params:Dict):
        # params = "&".join(f"{k}={v}" for k, v in params.items())
        params = urlencode(params)
        hashedsig = hmac.new(secretKey.encode("utf-8"), params.encode("utf-8"), hashlib.sha256).hexdigest()
        return hashedsig

    @overrides
    def prepare(self, apiCall:str, params:Dict, headers:Dict):
        url, params, headers = super().prepare(apiCall, params, headers)

        timestamp = int(time.time() * 1000)
        params["timestamp"] = timestamp
        logger.debug2(f"Timestamp: {timestamp}")

        hashedsig = SignedBinanceRequest.getRequestSignature(self.secret["secretKey"], params)
        params["signature"] = hashedsig
        logger.debug2("Hashed signature: {hashedsig}")

        headers["X-MBX-APIKEY"] = self.secret["apiKey"]
        logger.debug2(f"API key: {self.secret['apiKey']}")

        return url, params, headers

    @overrides
    def makeRequestRaw(self, apiCall:str, params:Dict, headers:Dict, requestType:str):
        result = super().makeRequestRaw(apiCall, params, headers, requestType)
        # Update the send timestamp using the value from the params for signed calls.
        result.timestamps["sendTimestamp"] = params["timestamp"]
        return result
